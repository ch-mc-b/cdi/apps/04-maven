Maven
=====

Maven, ein jiddisches Wort bedeutet Speicher des Wissens , begann als ein Versuch , die Build -Prozesse in der Jakarta Turbine Projekt zu vereinfachen.
 
Es gab mehrere Projekte mit jeweils eigenen Build-Dateien, die sich alle geringfügig voneinander unterschieden. Java-Libraries wurden in die Versionsverwaltung eingecheckt!?!

Wir wollten eine Standardmethode zum Erstellen der Projekte, eine klare Definition dessen, woraus das Projekt bestand, eine einfache Methode zum Veröffentlichen von Projektinformationen und eine Methode zum Teilen von Java-Libraries für mehrere Projekte.

Das Ergebnis ist ein Tool, mit dem Sie jetzt jedes Java-basierte Projekt erstellen und verwalten können. 

Eine Maven Umgebung ist in der Eclipse Theia IDE integriert. 

Die `Shell` und `Maven Befehle` sind in einem Terminal Fenster, in der IDE, auszuführen.
    
Beispiel Web Projekt
--------------------

Das nachfolgende Beispiel basieren auf dem Tutorial [Maven in 5 Minutes](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html) und ist auf die wichtigsten Befehle beschränkt.


Mittels einer Standard Template für ein Web Projekt erstellen wir ein Gerüst:

    cd /home/project/src
    mvn archetype:generate -DarchetypeArtifactId=jersey-quickstart-grizzly2 \
        -DarchetypeGroupId=org.glassfish.jersey.archetypes -DinteractiveMode=false \
        -DgroupId=ch.cdi -DartifactId=web-app -Dpackage=ch.cdi.webapp \
        -DarchetypeVersion=2.17
    cd web-app
    
Damit der Service erreichbar ist, müssen wir in der Datei `src/main/java/ch/cdi/webapp/Main.java` den Eintrag `localhost` auf `0.0.0.0` ändern.

    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://0.0.0.0:8080/myapp/";

Ausserdem sollte die `main` Methode, um die App später in einem Container ausführen zu können, wie folgt angepasst werden:

    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Web-App started available at "
                + "%smyresource\nCtrl+C to stop it...", BASE_URI));
    }

Anschliessend können wir das Projekt compilieren und starten:    
    
    mvn compile
    mvn exec:java
  
In einem neuen Terminalfenster, können wir die Funktion mittels `curl` (Windows: Invoke-Webrequest) testen:

    curl localhost:8080/myapp/myresource
     
Die Abhängigkeiten, von externen Libraries etc., können mittels `mvn dependency:tree` angezeigt werden.

Single Jar Datei
----------------

Für das Ablegen in einem Container, ist es am Einfachsten, wenn unsere Applikation als .jar Datei zur Verfügung steht.

In dieser .jar Datei wird das Programm mit allen Abhängigkeiten (Libraries etc.) gespeichert.

Dazu ist die Datei `pom.xml` wie folgt zu erweitern:

    <plugin>
    <artifactId>maven-assembly-plugin</artifactId>
    <configuration>
        <archive>
        <manifest>
            <mainClass>ch.cdi.webapp.Main</mainClass>
        </manifest>
        </archive>
        <descriptorRefs>
        <descriptorRef>jar-with-dependencies</descriptorRef>
        </descriptorRefs>
    </configuration>
    </plugin> 

Erstellung Single Jar Datei
---------------------------

Mit dem Build Tool Maven können wir, dann die Single Jar Datei erstellen.

    mvn clean compile assembly:single

Die Single Jar Datei können wir direkt mit Java ausführen:

    java -jar target/web-app-1.0-SNAPSHOT-jar-with-dependencies.jar

Und wie Oben beschrieben, testen.

    
